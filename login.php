<?php

        session_start();

        if(isset($_POST["mail"]) && isset($_POST["password"])){
            $mail = $_POST["mail"];
            $password = $_POST["password"];

        include ("db.php");

        //執行sql語法,將結果存放至$result
        $sql = "SELECT * FROM user WHERE mail = '$mail'";
        $result = mysqli_query($conn, $sql);

        //從$result中取出第一航資料作為一個陣列,並指定給$row,每當成功取出一行資料,即執行回圈內程式
        while($row = mysqli_fetch_array($result)) {
   if($row['1'] == $password){
       $_SESSION["user_mail"] = $row['0'];
       $_SESSION["user_name"] = $row['2'];
       $_SESSION["phone"] = $row['3'];
       $_SESSION["birthday"] = $row['4'];
       header("Location:index.html");

   }
}

        //如果$result中一行資料沒有
        if(mysqli_num_rows($result) == 0){
            $error = "<p>此帳號尚未註冊.</p>";
        }else{
            $error = "<p>密碼錯誤，請重新輸入.</p>";
        }

        //隨手關門
        mysqli_close($conn);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <style>
    hr.style-one {
                border: 0;
                height: 2px;
                background-image: linear-gradient(to right, rgba(0,0,0,0), rgb(76, 98, 106), rgba(0,0,0,0));
            }
        p{
            color: #d64f6a;
            font-family: 微軟正黑體;
            font-weight: 700;
            font-size: 13px;

        }
    </style>
</head>
<body>
   <section>
    <form class="box" class="login" method="post">
        <h1>L o g i n .</h1>
        <input type="email" name="mail" id="mail" placeholder="電子信箱" value="<?php echo $_POST["mail"] ?? "" ?>" required />

        <input type="password" name="password" id="password" placeholder="密碼" required/>
        <?php echo $error ?? "" ?>
        <hr class="style-one">
        <input type="submit" name="" value="Login">
        <br>


        <?php
                if(isset($_GET["alert"])){
                    ?>
                    <script> alert("<?php echo $_GET["alert"] ?>") </script>
                    <?php
                }
            ?>
    </form>
    </section>
</body>
</html>
